// ==UserScript==
// @id                  GmailAttachmentOnTop
// @name       			Gmail Attachment On Top
// @namespace           https://gitlab.com/Miliv/GmailAttachmentOnTop
// @version    			2.1
// @description			Move Gmail attachments to the top of your mail. Fork of https://openuserjs.org/scripts/imak1to/Gmail_attachment_on_top by
// @author              Daniel de Amilivia & https://openuserjs.org/users/imak1to
// @match               https://mail.google.com/mail/*
// @require             http://code.jquery.com/jquery-latest.js
// @grant               GM_addStyle
// ==/UserScript==

//PENDING: Iniciar oculto, agregar botton de Toggle, togglear sólo el correspondiente, icono

(function() {
    'use strict';

    if (window.top != window.self) { //don't run on frames or iframes
        return;
    }

    var observer = new MutationObserver(function() {
        if (document.getElementsByClassName('Bu')) {
            //if ( $('.aHl').children().length > 0 ) {
                console.log('table has appeared.');
                $(".hq").contents().appendTo(".aHl");
            //}
            $(".ho").off('click').on('click', ToggleAttachs);
            $(".f.gW").off('click').on('click', ToggleAttachs);
        }
    });
    observer.observe(document, {
        childList: true,
        subtree: true,
        attributes: false,
        characterData: false,
    });


    // Visual upgrades
    GM_addStyle(".gH { padding-right:16px; }");
    GM_addStyle(".aHl { margin-left: -2px !important; background: rgb(245, 245, 245); padding-left:16px; -webkit-border-radius:5px; -moz-border-radius:5px; border-radius:5px; padding-right:16px; border-bottom: 1px dashed #ccc;margin-bottom: 10px;}");
    GM_addStyle(".ho { cursor: pointer; margin: 0 !important; line-height: 30px; }");
    GM_addStyle(".aVW { font-size: 12px !important; font-weight: normal !important; height: unset !important; line-height: unset !important; }");
    //GM_addStyle(".aYp { font-size:8px!important;display:run-in!important;");
    GM_addStyle(".aQH { margin-bottom:0!important; }");

    function ToggleAttachs() {
        $(".aQH").toggle();
    }
})();